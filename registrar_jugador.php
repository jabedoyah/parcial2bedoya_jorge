<?php
require('configs/include.php');
class c_registrar_jugador extends super_controller{
    public function add(){
        $jugador = new jugador($this->post);        
        $this->orm->connect();
        $this->orm->insert_data("normal",$jugador);
        $this->orm->close();
        
        $this->type_warning = "success";
        $this->msg_warning = "Jugador agregada correctamente";
        
        $this->temp_aux = 'message.tpl';
        $this->engine->assign('type_warning', $this->type_warning);
        $this->engine->assign('msg_warning', $this->msg_warning);   
    }
    
    public function display(){
        $this->engine->display('header.tpl');
        $this->engine->display($this->temp_aux);
        $this->engine->display('registrar_jugador.tpl');
        $this->engine->display('footer.tpl');
    }
    
    public function run(){
        try {if (isset($this->get->option)){$this->{$this->get->option}();}}
        catch (Exception $e){
            $this->error = 1; 
            $this->engine->assign('object', $this->post);
            $this->msg_warning = $e->getMessage();
            $this->engine->assign('type_warning', $this->type_warning);
            $this->engine->assign('msg_warning', $this->msg_warning);
            $this->temp_aux = 'message.tpl';
        }
        $this->display();
    }
}

$call = new c_registrar_jugador();
$call->run();
?>