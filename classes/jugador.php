<?php
    class jugador extends object_standard{
        
        protected $cedula;
        protected $nombre;
        protected $posicion;
        protected $equipo;

        
        var $components = array();
        var $auxiliars = array();
        
        public function metadata(){
            return array("cedula" => array(), "nombre" => array(), "posicion" => array(), "equipo" => array("foreign_name" => "e_j", "foreign" => "equipo", "foreign_attribute" => "codigo"));
        }
        
        public function primary_key(){
            return array("cedula");
        }
        
        public function relational_keys($class, $rel_name){
            
            switch($class){
                case "equipo":
                    switch($rel_name){
                        case "e_j":
                            return array("equipo");
                            break;                    
                    }
                    break;
                default:
                    break;
            }
        }
    }
?>