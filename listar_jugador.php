<?php

require('configs/include.php');


class c_listar_jugador extends super_controller {
    

    public function display()
    {	
        
       
        $cod['equipo']['codigo'] = $this->post->codigo;
        $options['equipo']['lvl2']="one";

        $cod['jugador']['equipo'] = $this->post->codigo;
        $options['jugador']['lvl2']="by_equipo";

        $components['equipo']['jugador']=array("e_j");
        
        @$this->orm->connect();
        @$this->orm->read_data(array("equipo","jugador"),$options,$cod);
        @$equipo = $this->orm->get_objects("equipo",$components);
        $this->engine->assign('jugador', count($this->orm->get_data("jugador",0)));
        
        $this->orm->close();
        
        
        $this->engine->assign('equipo',$equipo[0]);
        $this->engine->assign('title','Mostrar jugadores');
        $this->engine->display('header.tpl');
        $this->engine->display('listar_jugador.tpl');
        $this->engine->display('footer.tpl');
    }

    public function run()
    {
            $this->display();
    }
}

$call = new c_listar_jugador();
$call->run();

?>